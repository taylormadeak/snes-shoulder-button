# CHANGELOG

## [rev_2a] 2022-10-23
### Changed:
- Board outline adjusted to loosen tolerances a bit and make for a better fit as a drop-in replacement.
- Footprints and symbols consolidated - schematic and pcb layout updated accordingly.
- License changed from BY-NC-SA to BY-SA.  Feel free to sell boards produced from this design, as long as you include the appropriate attribution.

## [rev_1c] 2022-04-24
Initial release version.